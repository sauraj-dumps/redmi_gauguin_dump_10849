## qssi-user 11 RKQ1.200826.002 V12.5.8.0.RJSMIXM release-keys
- Manufacturer: xiaomi
- Platform: lito
- Codename: gauguin
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.200826.002
- Incremental: V12.5.8.0.RJSMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/gauguin/gauguin:11/RKQ1.200826.002/V12.5.8.0.RJSMIXM:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.200826.002-V12.5.8.0.RJSMIXM-release-keys
- Repo: redmi_gauguin_dump_10849


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
